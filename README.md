## Docler Test - ToDo Task Manager

#### About the Project

A simple API to manage task. Its possible to create Tasks, edit Tasks and list all Tasks and also filter tasks by status or created date.
It was concept as a test interview for Docler Holding.

#### Runing the application

In order to run this application, you'll need to have docker installed

To start the application, please run the following command from application root:

Run docker up

```
$ docker-compose up
```

Run composer install

```
$ docker exec docler-php composer install
```

Finally, run the app migrations to create database

```
$ docker exec docler-php php artisan migrate:install

$ docker exec docler-php php artisan migrate
```

#### End-points

The application has three simple endpoints:

##### Create Task:

A POST method that create a single task that need a Json Object in the body with the task name and the description.

That validate task name and tasks with already inserted name will not be accept.
````
POST localhost/api/tasks
````
````
{
	"task": "TASK-1",
	"description": "Create the composition root"
}
````

##### List Tasks:

A GET method that list all tasks or filter by status or created date.

Can be call to list all tasks
````
GET localhost/api/tasks
````

Or filter the tasks by status

The Statuses that can be used are **"ToDo"**, **"Done"** and **"InProgess"** 
````
GET localhost/api/tasks?status=Done
````

Or filter by created date with full date
````
GET localhost/api/tasks?createdAt=2020-03-15
````

##### Update Tasks:

A PUT method that update a single task.

That expect a query param key with the name of Task to edit and body params with the new Status

````
PUT localhost/api/tasks/:key
````

The Statuses that can be used are **"ToDo"**, **"Done"** and **"InProgess"** 

````
{
	"status": "InProgress"
}
````

##### Postman Collection

If you wanna try the application using Postman, you can import the documentation from this link:
https://www.getpostman.com/collections/54c162d7a9c6c4fe7e6b

### Unit tests

To run the unit tests please execute the following command:

```
$ docker exec docler-php ./vendor/bin/phpunit 
```

Creted By Stefan Amaral

LinkedIn (https://www.linkedin.com/in/st%C3%A9fan-de-lima-amaral-13325982/)

e-mail: stefan.delima.amaral@gmail.com
