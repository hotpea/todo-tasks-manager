<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/tasks', '\App\Tasks\Web\CreateTask\CreateTaskController@create');
Route::put('/tasks/{key}', '\App\Tasks\Web\UpdateTask\UpdateTaskController@update');
Route::get('/tasks', '\App\Tasks\Web\ListTasks\ListTasksController@list');
