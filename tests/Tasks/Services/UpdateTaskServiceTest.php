<?php


namespace Tests\Tasks\Services;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Domain\Task;
use App\Tasks\Services\UpdateTask\IUpdateTaskRepository;
use App\Tasks\Services\UpdateTask\TaskNotFoundException;
use App\Tasks\Services\UpdateTask\UpdateTaskService;
use App\Tasks\Web\Commands\UpdateTaskCommand;
use PHPUnit\Framework\TestCase;
use DateTime;

class UpdateTaskServiceTest extends TestCase
{
    public function testItShouldNot_UpdateTask_WhenTaskNotFound()
    {
        $this->expectException(TaskNotFoundException::class);

        $service = new UpdateTaskService(new UpdateTaskRepository());
        $service->execute(new UpdateTaskCommand('Task-2', 'InProgress'));
    }
}

class UpdateTaskRepository implements IUpdateTaskRepository
{
    private array $tasks;

    public function __construct()
    {
        $this->tasks = [
            new Task('Task-1', new Description('new task'), Status::inProgress(), new DateTime()),
        ];
    }

    function find(string $task): ?Task
    {
        $tasks = array_filter($this->tasks, function ($t) use ($task) {
            return $task == $t->key;
        });

        return array_pop($tasks);
    }

    function update(Task $task): void
    {
    }
}
