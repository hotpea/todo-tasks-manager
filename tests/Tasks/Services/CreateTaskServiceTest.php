<?php


namespace Tests\Tasks\Services;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Domain\Task;
use App\Tasks\Services\CreateTask\CreateTaskService;
use App\Tasks\Services\CreateTask\ICreateTaskRepository;
use App\Tasks\Services\CreateTask\TaskAlreadyExistsException;
use App\Tasks\Web\Commands\CreateTaskCommand;
use PHPUnit\Framework\TestCase;
use DateTime;

class CreateTaskServiceTest extends TestCase
{
    public function testItShouldNot_CreateTask_WhenItAlreadyExists()
    {
        $this->expectException(TaskAlreadyExistsException::class);

        $service = new CreateTaskService(new CreateTaskRepository());
        $service->execute(new CreateTaskCommand('Task-1', 'New task'));
    }
}

class CreateTaskRepository implements ICreateTaskRepository
{
    private array $tasks;

    public function __construct()
    {
        $this->tasks = [
            new Task('Task-1', new Description('Title'), Status::toDo(), new DateTime())
        ];
    }

    function find(string $task): ?Task
    {
        $tasks = array_filter($this->tasks, function ($t) use ($task) {
            return $task == $t->key;
        });

        return array_pop($tasks);
    }

    function create(Task $task): void
    {
    }
}
