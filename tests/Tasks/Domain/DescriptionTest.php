<?php


namespace Tests\Tasks\Domain;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Descriptions\NullOrEmptyDescriptionException;
use App\Tasks\Domain\Descriptions\TooLongDescriptionException;
use PHPUnit\Framework\TestCase;

class DescriptionTest extends TestCase
{
    public function testItShouldNot_CreateDescription_WhenValueIsEmpty()
    {
        $this->expectException(NullOrEmptyDescriptionException::class);

        $description = new Description('');
    }

    public function testItShouldNot_CreateDescription_WhenValueIsTooLong()
    {
        $this->expectException(TooLongDescriptionException::class);

        $description = new Description('This task has too long description and that is why it should not be created.
        This task has too long description and that is why it should not be created.
        This task has too long description and that is why it should not be created.
        This task has too long description and that is why it should not be created.');
    }

    public function testItShould_CreateDescription_WhenValueIsAcceptable()
    {
        $value = 'This is a valid description';
        $description = new Description($value);

        $this->assertEquals($value, $description->value);
    }
}
