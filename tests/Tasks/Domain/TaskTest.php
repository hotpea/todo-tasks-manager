<?php


namespace Tests\Tasks\Domain;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Domain\Task;
use PHPUnit\Framework\TestCase;
use DateTime;

class TaskTest extends TestCase
{
    public function testItShould_CreateTask_WhenDataIsCorrect()
    {
        $task = new Task('Task-1', new Description('Just a task to do'), Status::toDo(), new DateTime());

        $this->assertEquals('Task-1', $task->key);
    }
}
