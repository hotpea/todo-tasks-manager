<?php


namespace Tests\Tasks\Domain;


use App\Tasks\Domain\Statuses\NullOrEmptyStatusException;
use App\Tasks\Domain\Statuses\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testItShouldNot_CreateStatusFromText_WhenItDoesNotExist()
    {
        $this->expectException(NullOrEmptyStatusException::class);

        $status = Status::fromText('notFound');
    }

    public function testItShould_CreateStatusFromText_WhenItExists()
    {
        $status = Status::toDo();

        $this->assertEquals('ToDo', $status->value);
    }
}
