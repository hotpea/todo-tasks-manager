<?php


namespace App\Tasks\Services\UpdateTask;


use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Web\Commands\ICommand;
use App\Tasks\Web\Commands\ICommandService;

class UpdateTaskService implements ICommandService
{
    private IUpdateTaskRepository $repository;

    public function __construct(IUpdateTaskRepository $repository)
    {
        $this->repository = $repository;
    }

    function execute(ICommand $command): void
    {
        $task = $this->repository->find($command->task);
        if ($task == null)
            throw new TaskNotFoundException;

        $task->updateStatus(Status::fromText($command->status));

        $this->repository->update($task);
    }
}
