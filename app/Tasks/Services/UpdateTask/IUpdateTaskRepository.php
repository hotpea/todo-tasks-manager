<?php


namespace App\Tasks\Services\UpdateTask;


use App\Tasks\Domain\Task;

interface IUpdateTaskRepository
{
    function find(string $task): ?Task;
    function update(Task $task): void;
}
