<?php


namespace App\Tasks\Services\CreateTask;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Domain\Task;
use App\Tasks\Web\Commands\ICommand;
use App\Tasks\Web\Commands\ICommandService;

use DateTime;

class CreateTaskService implements ICommandService
{
    private ICreateTaskRepository $repository;

    public function __construct(ICreateTaskRepository $repository)
    {
        $this->repository = $repository;
    }

    function execute(ICommand $command): void
    {
        $task = $this->repository->find($command->task);
        if ($task != null)
            throw new TaskAlreadyExistsException;

        $task = new Task(
            $command->task,
            new Description($command->description),
            Status::toDo(),
            new DateTime());

        $this->repository->create($task);
    }
}
