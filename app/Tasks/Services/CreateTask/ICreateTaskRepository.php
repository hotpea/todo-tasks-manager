<?php


namespace App\Tasks\Services\CreateTask;


use App\Tasks\Domain\Task;

interface ICreateTaskRepository
{
    function find(string $task): ?Task;
    function create(Task $task): void;
}
