<?php


namespace App\Tasks\Services;


use App\Tasks\Web\Queries\IQuery;
use App\Tasks\Web\Queries\IQueryService;
use App\Tasks\Web\Queries\IResult;

class QueryServiceMediator implements IQueryService
{
    /** @var IQueryService[] */
    private array $services;

    public function __construct(array $services)
    {
        $this->services = $services;
    }

    function execute(IQuery $query): IResult
    {
        $service = $this->services[get_class($query)];
        return $service->execute($query);
    }
}
