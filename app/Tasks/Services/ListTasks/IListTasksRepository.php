<?php


namespace App\Tasks\Services\ListTasks;


use DateTime;

interface IListTasksRepository
{
    function list(?string $status, ?DateTime $createdAt): array;
}
