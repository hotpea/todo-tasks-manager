<?php


namespace App\Tasks\Services\ListTasks;


use App\Tasks\Web\Queries\IQuery;
use App\Tasks\Web\Queries\IQueryService;
use App\Tasks\Web\Queries\IResult;
use App\Tasks\Web\Queries\ListTasksResult;

class ListTasksService implements IQueryService
{
    private IListTasksRepository $repository;

    public function __construct(IListTasksRepository $repository)
    {
        $this->repository = $repository;
    }

    function execute(IQuery $query): IResult
    {
        return new ListTasksResult($this->repository->list($query->status, $query->createdAt));
    }
}
