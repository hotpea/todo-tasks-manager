<?php


namespace App\Tasks\Services;


use App\Tasks\Web\Commands\ICommand;
use App\Tasks\Web\Commands\ICommandService;

class CommandServiceMediator implements ICommandService
{
    /** @var ICommandService[] */
    private array $services;

    public function __construct(array $services)
    {
        $this->services = $services;
    }

    function execute(ICommand $command): void
    {
        $service = $this->services[get_class($command)];
        $service->execute($command);
    }
}
