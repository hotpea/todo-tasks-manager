<?php


namespace App\Tasks\Infrastructure\PersistenceViaEloquentORM;


use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
