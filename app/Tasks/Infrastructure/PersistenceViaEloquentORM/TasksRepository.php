<?php


namespace App\Tasks\Infrastructure\PersistenceViaEloquentORM;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use App\Tasks\Domain\Task;
use App\Tasks\Services\CreateTask\ICreateTaskRepository;
use App\Tasks\Infrastructure\PersistenceViaEloquentORM\Task as TaskModel;
use App\Tasks\Services\ListTasks\IListTasksRepository;
use App\Tasks\Services\UpdateTask\IUpdateTaskRepository;
use DateTime;

class TasksRepository implements ICreateTaskRepository, IUpdateTaskRepository, IListTasksRepository
{
    function find(string $task): ?Task
    {
        $model = TaskModel::where('key', $task)->first();
        if ($model == null)
            return null;

        return new Task(
            $model->key,
            new Description($model->description),
            Status::fromText($model->status),
            new DateTime($model->createdAt));
    }

    function create(Task $task): void
    {
        $taskModel = new TaskModel;

        $taskModel->key = $task->key;
        $taskModel->description = $task->description->value;
        $taskModel->status = $task->status->value;
        $taskModel->createdAt = $task->createdAt;

        $taskModel->save();
    }

    function update(Task $task): void
    {
        $model = TaskModel::where('key', $task->key)->first();
        $model->status = $task->status->value;

        $model->save();
    }

    function list(?string $status, ?DateTime $createdAt): array
    {
        if ($status != null && $createdAt != null)
            return TaskModel::where('status', $status)->where('createdAt', '>', $createdAt)->get()->toArray();

        if ($status != null)
            return TaskModel::where('status', $status)->get()->toArray();

        if ($createdAt != null)
            return TaskModel::where('createdAt', '>', $createdAt)->get()->toArray();

        return TaskModel::all()->toArray();
    }
}
