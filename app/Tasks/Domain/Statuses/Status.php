<?php


namespace App\Tasks\Domain\Statuses;


class Status
{
    private string $value;

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function toDo(): Status
    {
        return new Status('ToDo');
    }

    public static function inProgress(): Status
    {
        return new Status('InProgress');
    }

    public static function done(): Status
    {
        return new Status('Done');
    }

    public static function fromText($value): Status
    {
        $statuses = [
            'ToDo' => Status::toDo(),
            'InProgress' => Status::inProgress(),
            'Done' => Status::done(),
        ];

        if (!isset($statuses[$value]))
            throw new NullOrEmptyStatusException("Status {$value} not found");

        return $statuses[$value];
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
