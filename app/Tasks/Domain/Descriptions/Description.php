<?php


namespace App\Tasks\Domain\Descriptions;


class Description
{
    private string $value;

    public function __construct(string $value)
    {
        if (empty($value))
            throw new NullOrEmptyDescriptionException("Description cannot be null or empty");

        if (strlen($value) > 200)
            throw new TooLongDescriptionException("Description must have less than 200 chars");

        $this->value = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
