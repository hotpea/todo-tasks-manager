<?php


namespace App\Tasks\Domain;


use App\Tasks\Domain\Descriptions\Description;
use App\Tasks\Domain\Statuses\Status;
use DateTime;

class Task
{
    private string $key;
    private Description $description;
    private Status $status;
    private DateTime $createdAt;

    public function __construct(string $key, Description $description, Status $status, DateTime $createdAt)
    {
        $this->key = $key;
        $this->description = $description;
        $this->status = $status;
        $this->createdAt = $createdAt;
    }

    public function updateStatus(Status $status)
    {
        $this->status = $status;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
