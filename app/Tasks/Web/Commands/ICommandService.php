<?php


namespace App\Tasks\Web\Commands;


interface ICommandService
{
    function execute(ICommand $command): void;
}
