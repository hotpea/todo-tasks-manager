<?php


namespace App\Tasks\Web\Commands;


class CreateTaskCommand implements ICommand
{
    use Getter;

    private string $task;
    private string $description;

    public function __construct(string $task, string $description)
    {
        $this->task = $task;
        $this->description = $description;
    }
}
