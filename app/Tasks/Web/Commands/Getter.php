<?php


namespace App\Tasks\Web\Commands;


trait Getter
{
    public function __get($name)
    {
        return $this->$name;
    }
}
