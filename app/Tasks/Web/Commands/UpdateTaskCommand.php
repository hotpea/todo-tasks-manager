<?php


namespace App\Tasks\Web\Commands;


class UpdateTaskCommand implements ICommand
{
    use Getter;

    private string $task;
    private string $status;

    public function __construct(string $task, string $status)
    {
        $this->task = $task;
        $this->status = $status;
    }
}
