<?php


namespace App\Tasks\Web\CreateTask;


use App\Http\Controllers\Controller;
use App\Tasks\Web\Commands\CreateTaskCommand;
use App\Tasks\Web\Commands\ICommandService;
use Illuminate\Http\Request;
use Exception;

class CreateTaskController extends Controller
{
    private ICommandService $createTask;

    public function __construct(ICommandService $createTask)
    {
        $this->createTask = $createTask;
    }

    public function create(Request $request)
    {
        try
        {
            $this->createTask->execute(new CreateTaskCommand($request->task, $request->description));

            return response()->json([
                'success' => true,
                'data' => [
                    'task' => $request->task
                ]
            ]);
        }
        catch (Exception $exception)
        {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }
}
