<?php


namespace App\Tasks\Web\UpdateTask;


use App\Http\Controllers\Controller;
use App\Tasks\Web\Commands\ICommandService;
use App\Tasks\Web\Commands\UpdateTaskCommand;
use Illuminate\Http\Request;
use Exception;

class UpdateTaskController extends Controller
{
    private ICommandService $updateTask;

    public function __construct(ICommandService $updateTask)
    {
        $this->updateTask = $updateTask;
    }

    public function update(Request $request)
    {
        try
        {
            $this->updateTask->execute(new UpdateTaskCommand($request->key, $request->status));

            return response()->json([
                'success' => true,
                'data' => [
                    'task' => $request->key,
                    'status' => $request->status
                ]
            ]);
        }
        catch (Exception $exception)
        {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }
}
