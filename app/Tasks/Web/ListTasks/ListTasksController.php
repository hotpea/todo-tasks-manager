<?php


namespace App\Tasks\Web\ListTasks;


use App\Http\Controllers\Controller;
use App\Tasks\Web\Queries\IQueryService;
use App\Tasks\Web\Queries\ListTasksQuery;
use Exception;
use Illuminate\Http\Request;
use DateTime;

class ListTasksController extends Controller
{
    private IQueryService $listsTasks;

    public function __construct(IQueryService $listTasks)
    {
        $this->listsTasks = $listTasks;
    }

    public function list(Request $request)
    {
        try
        {
            $createdAt = $request->createdAt != null ? new DateTime($request->createdAt) : null;
            $list = $this->listsTasks->execute(new ListTasksQuery($request->status, $createdAt));

            return response()->json([
                'success' => true,
                'data' => [
                    'tasks' => $list->tasks
                ]
            ]);
        }
        catch (Exception $exception)
        {
            return response()->json([
                'success' => false,
                'error' => $exception->getMessage()
            ]);
        }
    }
}
