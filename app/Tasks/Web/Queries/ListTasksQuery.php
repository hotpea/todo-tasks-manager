<?php


namespace App\Tasks\Web\Queries;


use DateTime;

class ListTasksQuery implements IQuery
{
    use Getter;

    private ?string $status;
    private ?DateTime $createdAt;

    public function __construct(?string $status, ?DateTime $createdAt)
    {
        $this->status = $status;
        $this->createdAt = $createdAt;
    }
}
