<?php


namespace App\Tasks\Web\Queries;


trait Getter
{
    public function __get($name)
    {
        return $this->$name;
    }
}
