<?php


namespace App\Tasks\Web\Queries;


interface IQueryService
{
    function execute(IQuery $query): IResult;
}
