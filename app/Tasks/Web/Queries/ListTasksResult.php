<?php


namespace App\Tasks\Web\Queries;


class ListTasksResult implements IResult
{
    private array $tasks;

    public function __construct(array $tasks)
    {
        $this->tasks = $tasks;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
