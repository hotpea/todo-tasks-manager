<?php

namespace App\Providers;

use App\Tasks\Infrastructure\PersistenceViaEloquentORM\TasksRepository;
use App\Tasks\Services\CommandServiceMediator;
use App\Tasks\Services\CreateTask\CreateTaskService;
use App\Tasks\Services\CreateTask\ICreateTaskRepository;
use App\Tasks\Services\ListTasks\IListTasksRepository;
use App\Tasks\Services\ListTasks\ListTasksService;
use App\Tasks\Services\QueryServiceMediator;
use App\Tasks\Services\UpdateTask\IUpdateTaskRepository;
use App\Tasks\Services\UpdateTask\UpdateTaskService;
use App\Tasks\Web\Commands\CreateTaskCommand;
use App\Tasks\Web\Commands\ICommandService;
use App\Tasks\Web\Commands\UpdateTaskCommand;
use App\Tasks\Web\Queries\IQueryService;
use App\Tasks\Web\Queries\ListTasksQuery;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ICreateTaskRepository::class, TasksRepository::class);
        $this->app->bind(IUpdateTaskRepository::class, TasksRepository::class);
        $this->app->bind(IListTasksRepository::class, TasksRepository::class);

        $this->app->bind(ICommandService::class, function ($app) {
            return new CommandServiceMediator([
                CreateTaskCommand::class => new CreateTaskService($this->app->get(ICreateTaskRepository::class)),
                UpdateTaskCommand::class => new UpdateTaskService($this->app->get(IUpdateTaskRepository::class)),
            ]);
        });

        $this->app->bind(IQueryService::class, function ($app) {
            return new QueryServiceMediator([
                ListTasksQuery::class => new ListTasksService($this->app->get(IListTasksRepository::class)),
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
